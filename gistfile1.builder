MigBuilder.build(:panel, :layout => [nil, "[right]"]) do |b|
             b.add build(:label, "General"), :split, :span, :gap_top => 10
             b.add build(:separator), :growx, :wrap, :gap_top => 10

             b.add build(:label, "Company"), :gap => 10
  @company = b.add build(:text_field, ""),  :span, :growx

             b.add build(:label, "contact"), :gap => 10
  @contact = b.add build(:text_field, ""), :span, :growx, :wrap


             b.add build(:label, "Propeller"), :split, :span, :gap_top => 10
             b.add build(:seperator), :growx, :wrap, :gap_top => 10

             b.add build(:label, "PTI/kW"), :gap => 10
  @PTIkW =   b.add build(:text_field, 10)

             b.add build(:label, "Power/kW"), :gap => 10
  @powKW =   b.add build(:text_field, 10), :wrap

             b.add build(:label, "R/mm"), :gap => 10
  @Rmm   =   b.add build(:text_field, 10)

             b.add build(:label, "D/mm"), :gap => 10
  @Dmm   =   b.add build(:text_field, 10), :wrap
end
